"The most accessible and sophisticated environmental monitoring equipment is made available to most humans in the forms of noses,
eyes, tongues, ears and skin". Sara Sage.


# 1- THE LOG

We have created a log for offline data taking (with paper and pen) to register and keep track of bad odor events. 
You can download a printable PDF version of the log or an editable version (with Draw.io) in the case you want to modify it, 
add content, remove logos... 


# 2- WHAT WE WANT TO DO?

To facilitate monitoring of bad odor events through the collection of data using a written odor log and an online map.


# 3- SUGGESTED ACTIVITY

You can use de log as you want and we encourage everyone to engage in a collective effort to improve the log and it's potential uses. 
This is just a suggestion to begin testing:

   * → Take at least a 2 week long daily log.
*    → Note on the log every day even if theres's no bad odor event.
*    → Coordinate yourself with some neighbours and do it simultaneously in different points of your area.
*    → You can then send the results to your local authorities carbon copied to some newspaper and the EPA or the environmental authority in your 
       country. Doing this, a track of your public complaint will be kept in several entities and mail servers. You can also carbon copy us at imvec@tutanota.com.


This way you'll go from "there are lots of bad odor events in my area" to more precise "we've counted xxxx number of bad odor events located
in A, B, C, and D places"... toghether with the geolocation, weather conditions, effects on people and all data that you decide to include
in the log. 


# 4- NEXT STEPS

Implement an online form in collaboration with the Emapic project, based on the Odor Log data, able to display concentrations of odor events 
on a interactive map together with the bad odor reports collected data.